import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   Tnode (String s){
      this.name = s;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(name);
      if (firstChild != null){
         b.append("(");
         b.append(firstChild);
         b.append(",");
      }
      if (nextSibling != null){
         b.append(nextSibling);
         b.append(")");
      }
      // TODO!!!
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<Tnode>();
      String operators = "- + / *";
      String[] elements = pol.split("\\s+");
      for (String s : elements){
         if (!s.matches(".*\\d+.*|[+\\-*/]|SWAP|ROT"))
            throw new RuntimeException("Illegal symbol " + s + " in " + pol);
         if (s.length() != 0) {
            Tnode root = new Tnode(s);
            if (s.equals("SWAP")) {
               try {
                  Tnode r = stack.pop();
                  Tnode l = stack.pop();
                  stack.add(r);
                  stack.add(l);
                  continue;
               } catch (Exception E) {
                  throw new RuntimeException("Too few parameters for SWAP in " + pol);
               }
            }
            else if (s.equals("ROT")){
               try {
                  Tnode a = stack.pop();
                  Tnode b = stack.pop();
                  Tnode c = stack.pop();
                  stack.add(b);
                  stack.add(a);
                  stack.add(c);
                  continue;
               } catch (Exception E){
                  throw new RuntimeException("Too few parameters for ROT in " + pol);
               }
            }
               if (operators.contains(s)) {
               try {
                  root.nextSibling = stack.pop();
                  root.firstChild = stack.pop();
               } catch (Exception e){
                  throw new RuntimeException("Not enough numbers in " + pol);
               }
            }
            stack.push(root);
         }
      }
      if (stack.size() != 1)
         throw new RuntimeException("Too few operators in " + pol);
      // TODO!!!
      return stack.pop();
   }


   public static void main (String[] param) {
      String rpn = "2 5 9 ROT + SWAP -";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }


   //Viited: https://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
}

